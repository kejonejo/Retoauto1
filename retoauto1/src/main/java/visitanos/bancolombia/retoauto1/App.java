package visitanos.bancolombia.retoauto1;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {

	private Properties parametrosEntrada;

	public static void main(String[] args) {

		App app = new App();
		app.cargarPropiedades();
		System.setProperty(app.parametrosEntrada.getProperty("nombreVariableSistema"),
				app.parametrosEntrada.getProperty("valorVariableSistema"));
		WebDriver driver = new ChromeDriver();
		Visitanos pag = new Visitanos(driver);
		pag.abrirUrl(app.parametrosEntrada);
		BuscarSucursal bs = new BuscarSucursal(driver);
		bs.ubicarOficina(app.parametrosEntrada);

	}

	private void cargarPropiedades() {

		InputStream cargarArchivo = this.getClass().getResourceAsStream("parametrosEntrada.properties");
		try {
			parametrosEntrada = new Properties();
			parametrosEntrada.load(cargarArchivo);
			cargarArchivo.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Error al Cargar Archivo de Propiedades." + e1);
		}
	}

}
