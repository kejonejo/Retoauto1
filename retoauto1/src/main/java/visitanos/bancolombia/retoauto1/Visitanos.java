package visitanos.bancolombia.retoauto1;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Visitanos {
	
	private WebDriver webdriver;
	private Properties datos;
	
	
	public Visitanos(WebDriver driver) {
		
		this.webdriver = driver;
		this.datos = new Properties();
		cargarPropiedades();
	}

	private void cargarPropiedades() {
		
		InputStream cargarArchivo = this.getClass().getResourceAsStream("datos.properties");
		try {
			datos.load(cargarArchivo);
			cargarArchivo.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Error al Cargar Archivo de Propiedades."+e1);
		}
	}

	public void abrirUrl(Properties datosEntrada) {
		webdriver.get(datosEntrada.getProperty("Url"));
		webdriver.findElement(By.xpath(datos.getProperty("visitanos"))).click();
		
		// TODO Auto-generated constructor stub
	}

}
