package visitanos.bancolombia.retoauto1;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BuscarSucursal {

	private WebDriver webdriver;
	private Properties datos;

	public BuscarSucursal(WebDriver driver) {
		// TODO Auto-generated constructor stub

		this.webdriver = driver;
		this.datos = new Properties();
		cargarPropiedades();

	}

	private void cargarPropiedades() {

		InputStream cargarArchivo = this.getClass().getResourceAsStream("datos.properties");
		try {
			datos.load(cargarArchivo);
			cargarArchivo.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Error al Cargar Archivo de Propiedades." + e1);
		}
	}

	public void ubicarOficina(Properties datosEntrada) {
		webdriver.findElement(By.xpath(datos.getProperty("ubicacion"))).sendKeys(datosEntrada.getProperty("direccion"));
		webdriver.findElement(By.xpath(datos.getProperty("buscar"))).click();
		esperar(5000);

		webdriver.findElement(By.xpath(datos.getProperty("mapa"))).click();

		// TODO Auto-generated constructor stub
	}

	private void esperar(long tiempo) {

		try {
			Thread.sleep(tiempo);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("Error al realizar la Espera." + e);
		}

	}

}
